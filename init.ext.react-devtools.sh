# See also: https://github.com/facebook/react-devtools
REACT_DEVTOOLS_DIR=_placement.react-devtools
TARGET_BRANCH=v3

git clone --branch=$TARGET_BRANCH https://github.com/facebook/react-devtools.git $REACT_DEVTOOLS_DIR &&
yarn --cwd $REACT_DEVTOOLS_DIR &&
yarn --cwd $REACT_DEVTOOLS_DIR build:extension &&
echo "👌 Extension built: React DevTools: $TARGET_BRANCH"
