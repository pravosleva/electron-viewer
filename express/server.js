/* eslint-disable global-require */
const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const cors = require('cors')
const path = require('path')

// --- ENVS
// require('dotenv').config(fs.readFileSync('.crm.env'))
const result = require('dotenv').config({
  path: path.resolve(process.cwd(), '.crm.env'),
})

if (result.error) {
  console.log(result.error)
  throw new Error('envs fuckup')
}
// ---

// --- CREATE FS
const { FILE_STORAGE_DIR, SP_API_INSTANCE } = process.env

if (!FILE_STORAGE_DIR) {
  console.error('🚫 ERR: FILE_STORAGE_DIR was not set, check .crm.env')
  throw new Error('FILE_STORAGE_DIR')
}

const fileStoragePath = require('./utils/getFileStoragePath')()

if (!fs.existsSync(fileStoragePath)) fs.mkdirSync(fileStoragePath)
// ---

// const fapiRouter = require('./router/fapi')

const isDev = process.env.NODE_ENV === 'development'
const CONFIG = {
  EXPRESS_SERVER_PORT: 3536,
}

// --- EXPRESS APP
const server = express()

// -- CORS
const corsOptions = {
  origin: SP_API_INSTANCE,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}
if (isDev) server.use(cors(corsOptions))
// --

// parse application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
server.use(bodyParser.json())
// server.use('/fapi', fapiRouter)
server.use(express.static('frontend/build'))

const expressServer = () =>
  server.listen(CONFIG.EXPRESS_SERVER_PORT, () => {
    console.log(
      `🔥 Express server listening on http://localhost:${CONFIG.EXPRESS_SERVER_PORT}`
    )
  })
// ---

module.exports = expressServer
