/* eslint-disable node/no-unpublished-require */
/* eslint-disable import/no-extraneous-dependencies */
// const { remote, BrowserWindow } = require('electron')

const makeCounter = (initVal = 0) => {
  let val = initVal

  return () => {
    val += 1
    return val
  }
}
const logCounter = makeCounter()

module.exports = {
  'channel.main': (_e, args) => {
    console.log(`#${logCounter()}`, args)
  },
}
