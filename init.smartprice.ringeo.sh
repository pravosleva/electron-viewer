# See also: https://github.com/facebook/react-devtools
PLACEMENT_DIR=_placement.smartprice.ringeo
TARGET_BRANCH=master

git clone --branch=$TARGET_BRANCH git@gitlab.com:smartprice/ringeo.git $PLACEMENT_DIR &&
# yarn --cwd $PLACEMENT_DIR &&
echo "👌 Git cloned to $PLACEMENT_DIR" &&
cd $PWD/$PLACEMENT_DIR &&
python3.6 -m venv venv &&
. venv/bin/activate &&
## TODO: which python3.6 - move to .envs
# which pip3 &&
# pip3 freeze &&
pip3 install -r ./front/requirements.txt

## NOTE: 2021.03.31

# python3 manage.py runserver
# sudo apt install libpq-dev
# which venv
# cat /etc/lsb-release
# sudo add-apt-repository ppa:deadsnakes/ppa
# sudo apt-get update && sudo apt-get install python3.6
# python3.6 -m venv venv
# sudo apt-get install python3.6-venv
# python3.6 -m venv venv
# . venv/bin/activate
# which pip3
# du -h venv
# pip3 freeze
# sudo apt install libcurl4-gnutls-dev
# sudo apt-get install python3.6-dev
# sudo apt-get install ldap-dev
# sudo apt-get install python3.6-ldap
# sudo apt-get install libldap2-dev
# sudo apt-get install libsasl2-dev
# sudo apt-get install libgnutls28-dev
# pip3 install -r front/requirements.txt
# sudo apt-get install geoip2
# sudo apt-get install libgeoip2
# sudo apt install geoipupdate

## STEP 1: OPEN TCP PORTS:
# ssh -L15432:10.8.73.12:5432 -L1389:10.8.73.12:389 -L16579:10.8.73.12:16579 dev
## OR:
# ssh -L15432:10.8.73.12:5432 -L1389:10.8.73.12:389 -L16579:10.8.73.12:16579 pravosleva@beta1.smartprice.ru

## STEP 2: RUN:
# LDAP_URI=ldap://localhost:1389 PG_HOST=localhost PG_PORT=15432 REDIS_URL='redis://:VE9u32Axfr7v4bAjTdaEBVZZgw6s2fDYYTXjp8C7@localhost:16579' IS_DEV=1 RUNTIME_DATA_DIR=/home/pravosleva/projects/smartprice_projects/ssr/runtime_data ./front/manage.py runserver
# LDAP_URI=ldap://localhost:1389 PG_HOST=localhost PG_PORT=15432 REDIS_URL='redis://:VE9u32Axfr7v4bAjTdaEBVZZgw6s2fDYYTXjp8C7@localhost:16579' IS_DEV=1 RUNTIME_DATA_DIR=/home/pravosleva/projects/smartprice_projects/ssr/runtime_data /home/pravosleva/projects/smartprice_projects/electron-viewer/_placement.smartprice.ringeo/venv/bin/python ./front/manage.py runserver
