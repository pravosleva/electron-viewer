/* eslint-disable import/no-dynamic-require */
const { ipcRenderer } = require('electron')
const path = require('path')

const { TARGET } = process.env

// NOTE: Special for jQuery
// console.log(module)
if (typeof module === 'object') {
  window.module = module
  // eslint-disable-next-line no-global-assign
  // module = undefined

  // eslint-disable-next-line no-multi-assign
  require(path.join(__dirname, 'assets/jq.ringeo.js'))
}

window.logEnvs = () => {
  // eslint-disable-next-line node/no-unsupported-features/node-builtins
  console.table({ ...process.env })
}
window.ipcRenderer = ipcRenderer

class Win {
  constructor(url, ...restArgsForWindow) {
    this.url = url
    this.win = window.open(this.url, ...restArgsForWindow)
  }

  tryEval() {
    try {
      this.win.eval(
        "document.getElementByTagName('form').style.fontFamily = 'Comic Sans MS';"
      )
    } catch (err) {
      console.error(err)
    }
  }
}
if (TARGET === 'SSR') {
  window.goOtSvyaznoy = () => {
    window.location.href = '/ot/svyaznoy'
  }
}

window.make = (url = '/') => {
  const w = new Win(url, '_blank', 'width=700,height=500,alwaysOnTop=1')

  // console.log(w)

  ipcRenderer.send('channel.main', `New window ${url} should be opened`)
}
