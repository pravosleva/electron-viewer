DEFAULT_SP_API_INSTANCE=http://beta1.smartprice.ru:9005
# DEFAULT_SP_API_INSTANCE=https://ringeo.ru
CRM_PLACEMENT_DIR_NAME=_placement.crm
CRM_PLACEMENT_DIR=$PWD/$CRM_PLACEMENT_DIR_NAME

read_var() {
  if [ -z "$1" ]; then
    echo "environment variable name is required"
    return 1
  fi

  local ENV_FILE='.env.crm'
  if [ ! -z "$2" ]; then
    ENV_FILE="$2"
  fi

  local VAR=$(grep $1 "$ENV_FILE" | xargs)
  IFS="=" read -ra VAR <<< "$VAR"
  echo ${VAR[1]}
}

if [ $# -eq 1 ]
then
  case $1 in
    "dev")

      # Step 0: Checking local CRM repo & clone if necessary
      CRM_DIR=$CRM_PLACEMENT_DIR/ui/ringeo_crm
      echo "Chechking: $CRM_DIR"
      if [ ! -d $CRM_PLACEMENT_DIR ]
      then
        echo "Does not exists: $CRM_PLACEMENT_DIR" &&

        # Step 1: Clone CRM and install deps:
        # https://gitlab.com/smartprice/ringeo-crm-front.git
        git clone --branch=feature/IT-2028 git@gitlab.com:smartprice/ringeo-crm-front.git $CRM_PLACEMENT_DIR_NAME &&
        if [ -d $CRM_DIR ]
        then
          echo "Created, exists: $CRM_DIR"
        fi
      else
        echo "Аlready exists: $CRM_PLACEMENT_DIR" &&
        echo "CRM deps installing..." &&
        yarn --cwd $CRM_DIR
      fi

      # Detect DEV_CSRF_TOKEN or should be set by user:
      OLD_DEV_CSRF_TOKEN=$(read_var DEV_CSRF_TOKEN)
      if [ ! ${OLD_DEV_CSRF_TOKEN} ]
      then
        echo "DEV_CSRF_TOKEN is empty" &&
        read -p "Enter DEV_CSRF_TOKEN: " DEV_CSRF_TOKEN &&
        if [ ! ${DEV_CSRF_TOKEN} ]
        then
          echo "🚫 ERR: DEV_CSRF_TOKEN is empty" &&
          exit 1
        fi
      else
        echo "DEV_CSRF_TOKEN is exists: $OLD_DEV_CSRF_TOKEN" &&
        while true; do
          read -p "Use this? (y|n) " yn
          case $yn in
            [Yy]* ) DEV_CSRF_TOKEN=$OLD_DEV_CSRF_TOKEN; break;;
            [Nn]* )
              read -p "Enter DEV_CSRF_TOKEN: " DEV_CSRF_TOKEN &&
              if [ ! ${DEV_CSRF_TOKEN} ]
              then
                echo "🚫 ERR: DEV_CSRF_TOKEN is empty"
                exit 1
              fi
              break;;
            * ) echo "Please answer yes or no.";;
          esac
        done
      fi

      # Step 2: Init envs for Electron app:
      # Detect API endpoint or should be set by user:
      OLD_SP_API_INSTANCE=$(read_var SP_API_INSTANCE)
      if [ ! ${OLD_SP_API_INSTANCE} ]
      then
        echo "SP_API_INSTANCE is empty" &&
        read -p "Enter SP_API_INSTANCE (or will be set $DEFAULT_SP_API_INSTANCE): " SP_API_INSTANCE &&
        if [ ! ${SP_API_INSTANCE} ]
        then
          SP_API_INSTANCE=$DEFAULT_SP_API_INSTANCE &&
          echo "SP_API_INSTANCE is $SP_API_INSTANCE"
        fi
      else
        echo "SP_API_INSTANCE is exists: $OLD_SP_API_INSTANCE" &&
        while true; do
          read -p "Use this? (y|n) " yn
          case $yn in
            [Yy]* ) SP_API_INSTANCE=$OLD_SP_API_INSTANCE; break;;
            [Nn]* )
              read -p "Enter SP_API_INSTANCE (or will be set $DEFAULT_SP_API_INSTANCE): " SP_API_INSTANCE &&
              yn=y &&
              if [ ! ${SP_API_INSTANCE} ]
              then
                SP_API_INSTANCE=$DEFAULT_SP_API_INSTANCE &&
                echo "SP_API_INSTANCE is $SP_API_INSTANCE"
              fi
              break;;
            * ) echo "Please answer yes or no.";;
          esac
        done
      fi

      # Detect DEV_SP_AUTH_SESS_ID or should be set by user:
      OLD_DEV_CSRF_TOKEN=$(read_var DEV_SP_AUTH_SESS_ID)
      if [ ! ${OLD_DEV_CSRF_TOKEN} ]
      then
        echo "DEV_SP_AUTH_SESS_ID is empty" &&
        read -p "Enter DEV_SP_AUTH_SESS_ID: " DEV_SP_AUTH_SESS_ID &&
        if [ ! ${DEV_SP_AUTH_SESS_ID} ]
        then
          echo "🚫 ERR: DEV_SP_AUTH_SESS_ID is empty" &&
          exit 1
        fi
      else
        echo "DEV_SP_AUTH_SESS_ID is exists: $OLD_DEV_CSRF_TOKEN" &&
        while true; do
          read -p "Use this? (y|n) " yn
          case $yn in
            [Yy]* ) DEV_SP_AUTH_SESS_ID=$OLD_DEV_CSRF_TOKEN; break;;
            [Nn]* )
              read -p "Enter DEV_SP_AUTH_SESS_ID: " DEV_SP_AUTH_SESS_ID &&
              if [ ! ${DEV_SP_AUTH_SESS_ID} ]
              then
                echo "🚫 ERR: DEV_SP_AUTH_SESS_ID is empty"
                exit 1
              fi
              break;;
            * ) echo "Please answer yes or no.";;
          esac
        done
      fi

      echo "FILE_STORAGE_DIR=file-storage
DEV_CSRF_TOKEN=$DEV_CSRF_TOKEN
SP_API_INSTANCE=$SP_API_INSTANCE
CRM_DIR=$CRM_DIR
PM2_PROCESS_NAME=crm-3001-dev
DEV_SP_AUTH_SESS_ID=$DEV_SP_AUTH_SESS_ID" > .env.crm &&
      echo "const fs = require('fs')

require('dotenv').config(fs.readFileSync('.env.crm'))

const { CRM_DIR, PM2_PROCESS_NAME } = process.env

if (!CRM_DIR) throw new Error('Check envs: CRM_DIR was not provided')

module.exports = {
  apps: [
    {
      name: PM2_PROCESS_NAME,
      cwd: CRM_DIR,
      script: 'yarn',
      args: 'rdev',
      interpreter: 'none',
      // env: {},
    },
  ],
}" > ecosystem.dev.crm.config.js &&

      # Step 3: Init envs for CRM:
      echo "API_URL=$SP_API_INSTANCE/api/
NODE_ENV=development
DEV_SERVER_PORT=3001
API_PORT=16909
STATIC_PORT=16909
DEV_SERVER=localhost" > $CRM_DIR/rdev.env
    ;;
    *)
    echo "🚫 ERR: envs-init.sh | Undefined param value" &&
    exit 1
  esac
  exit 0
else
  echo "🚫 ERR: envs-init.sh | Param is required: dev"
  exit 1
fi