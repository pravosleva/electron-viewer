// Modules to control application life and create native browser window
const {
  app,
  BrowserWindow,
  ipcMain,
  Menu,
  session,
  shell,
  Tray,
} = require('electron')
const path = require('path')
const cmd = require('node-cmd')
const pm2 = require('pm2')
const contextMenu = require('electron-context-menu')
const { createPollingByConditions } = require('./polling-to-frontend')

const { TARGET, NODE_ENV } = process.env

if (!TARGET) throw new Error('envs fuckup: !TARGET')

const isDev = NODE_ENV === 'development'
const state = new Map()
/* NOTE: For example
  - Map[PM2_PROCESS_NAME]
    { pid: 26299,
      name: 'crm-3001-dev',
      pm2_env: // etc.
      pm_id: 0,
      monit: { memory: 66260992, cpu: 100 } }
  - Map.windowCfg
  - Map.mainWindow
*/
const delay = (ms = 1000) =>
  new Promise((res, _rej) => {
    setTimeout(res, ms)
  })

const windowCfg = {
  minWidth: 425,
  minHeight: 700,
  width: 900,
  height: 500,
  // frame: false,
  webPreferences: {
    preload: path.join(__dirname, 'preload.js'),
    nodeIntegration: true,
    enableRemoteModule: true,
    webSecurity: false,
    contextIsolation: true,

    // See also: https://www.howtogeek.com/179070/how-to-get-access-to-experimental-features-on-your-chromebook-or-just-in-chrome/
    // experimentalFeatures: true,

    // scrollBounce: true,
    // backgroundColor: '#3882c4',
    backgroundColor: '#404040',
  },
  darkTheme: true,
  plugins: true,
  sandbox: true,
}
if (isDev) {
  windowCfg.width = 1200
  windowCfg.height = 800
}

// --- SET ENVS
const envMap = {
  SSR: {
    ENV_CFG_FILE: '.env.ssr',
  },
  CRM: {
    ENV_CFG_FILE: '.env.crm',
  },
  RINGEO: {
    ENV_CFG_FILE: '.env.smartprice.ringeo',
  },
}
if (!envMap[TARGET])
  throw new Error(
    `envMap cfg предназначен только для следующих значений process.env.TARGET: ${Object.keys(
      envMap
    ).join(', ')}`
  )
try {
  const envs = require('dotenv').config({
    path: path.resolve(process.cwd(), envMap[TARGET].ENV_CFG_FILE),
  })

  if (envs?.error) {
    console.log(envs?.error)
    throw new Error('envs fuckup')
  }
} catch (err) {
  console.log(err)
}
// ---

const {
  DEV_CSRF_TOKEN,
  PM2_PROCESS_NAME,
  RUN_PM2,
  SP_API_INSTANCE,
  DEV_SP_AUTH_SESS_ID,
} = process.env
const enableBrowserExts = () => {
  // --- Chrome extensios:
  try {
    // -- Local unpacked exts:
    // ;[
    //   // NOTE: React Dev Tools V3:
    //   '/_placement.react-devtools/shells/chrome/build/unpacked',
    // ].forEach((localPath) => {
    //   ses
    //     .loadExtension(path.join(__dirname, localPath))
    //     .then(({ name }) => console.log(`👌 Added Extension: ${name}`))
    //     .catch((err) => console.log('⚡ An error occurred: ', err))
    // })
    // --

    // Load React Dev Tools:
    const {
      default: installExtension,
      // NOTE: React Dev Tools V4:
      REACT_DEVELOPER_TOOLS,
      REDUX_DEVTOOLS,
    } = require('electron-devtools-installer')
    ;[REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS].forEach((extension) => {
      installExtension(extension)
        .then((name) => console.log(`👌 Added Extension: ${name}`))
        .catch((err) => console.log('⚡ An error occurred: ', err))
    })
  } catch (err) {
    console.log(err)
  }
  // ---
}
const CONFIG = {
  SSR: {
    PM2_PROCESS_KEY: 'ssrProcess',
    FRONTEND_DEV_URL: 'http://localhost:3000',
    FRONTEND_FIRST_CONNECT_INTERVAL: 10000,
    FRONTEND_FIRST_CONNECT_METHOD: 'get',
    cb: (mainWindow) => {
      const ses = session.defaultSession
      const smartpriceDevCookies = {
        csrftoken: DEV_CSRF_TOKEN,
        spAuthSessId: DEV_SP_AUTH_SESS_ID,
      }

      for (const key in smartpriceDevCookies) {
        ses.cookies
          .set({
            value: smartpriceDevCookies[key],
            name: key,
            url: SP_API_INSTANCE,
          })
          .then(() => {
            console.log(`set cookie OK: ${key}`)
          })
          .catch((err) => {
            console.log(`set cookie FAIL: ${key}`)
            if (err.message) console.log(err.message)
          })
      }
      const filterReqsForCustomMutations = {
        urls: ['*://beta1.smartprice.ru/*'],
      }
      ses.webRequest.onBeforeSendHeaders(
        filterReqsForCustomMutations,
        (details, callback) => {
          // NOTE: details.requestHeaders.Cookie already set.
          // details.requestHeaders.Origin = SP_API_INSTANCE
          details.requestHeaders['X-CSRFToken'] = DEV_CSRF_TOKEN
          details.requestHeaders.Referer = `${SP_API_INSTANCE}${details.referrer.slice(
            `${CONFIG[TARGET].FRONTEND_DEV_URL}/`.length - 1
          )}`
          callback({ requestHeaders: details.requestHeaders })
        }
      )

      mainWindow.loadURL('http://localhost:3000')
      enableBrowserExts()
    },
  },
  CRM: {
    PM2_PROCESS_KEY: 'crmProcess',
    // EXPRESS_SERVER_PORT: 3536,
    FRONTEND_DEV_URL: 'http://localhost:3001',
    FRONTEND_FIRST_CONNECT_INTERVAL: 5000,
    FRONTEND_FIRST_CONNECT_METHOD: 'get',
    cb: (mainWindow) => {
      // --- Cookies
      const ses = session.defaultSession

      // const logCookies = () => { ses.cookies.get({}).then(console.log).catch(console.log) }
      const smartpriceDevCookies = {
        csrftoken: DEV_CSRF_TOKEN,
        // utm_source: encodeURIComponent(SP_API_INSTANCE),
        // _gcl_au: '1.1.1180763558.1606490985',
        // _vwo_uuid_v2:
        //   'D76F5C12A39277F50F7D8F2066ED8ED40|67cb3782e6064a8ab524cf5c88ab56b6',
        // _cmg_csstkF4us: '1613118655',
        // _comagic_idkF4us: '3728165770.6196341196.1613118654',
        // advcake_session_id: '747aa187-6f0b-7544-d6a9-b4a8cfd753af',
        // spuid_dev: '179313241612795499',
        spAuthSessId: DEV_SP_AUTH_SESS_ID,
        // rc_uid: 'bcb76d76-a7af-4299-ba75-3dc2e4920a05',
        // only_existing_experiment: '1',
      }

      for (const key in smartpriceDevCookies) {
        ses.cookies
          .set({
            value: smartpriceDevCookies[key],
            name: key,
            url: SP_API_INSTANCE,
          })
          .then(() => {
            console.log(`set cookie OK: ${key}`)
          })
          .catch((err) => {
            console.log(`set cookie FAIL: ${key}`)
            if (err.message) console.log(err.message)
          })
      }

      const filterReqsForCustomMutations = {
        urls: [
          // NOTE: For example: '*://*.google.com/*',
          '*://beta1.smartprice.ru/*',
        ],
      }
      ses.webRequest.onBeforeSendHeaders(
        filterReqsForCustomMutations,
        (details, callback) => {
          // NOTE: details.requestHeaders.Cookie already set.
          details.requestHeaders.Origin = SP_API_INSTANCE
          details.requestHeaders['X-CSRFToken'] = DEV_CSRF_TOKEN
          details.requestHeaders.Referer = `${SP_API_INSTANCE}${details.referrer.slice(
            `${CONFIG[TARGET].FRONTEND_DEV_URL}/`.length - 1
          )}`
          callback({ requestHeaders: details.requestHeaders })
        }
      )

      // mainWindow.webContents.on('did-finish-load', () => { logCookies() })
      // ---

      enableBrowserExts()

      // mainWindow.loadFile('./index.crm.html')
      mainWindow.loadURL(CONFIG[TARGET].FRONTEND_DEV_URL)
    },
  },
  RINGEO: {
    PM2_PROCESS_KEY: 'spRingeoProcess',
    // EXPRESS_SERVER_PORT: 3536,
    FRONTEND_DEV_URL: 'http://localhost:8000',
    FRONTEND_FIRST_CONNECT_INTERVAL: 5000,
    FRONTEND_FIRST_CONNECT_METHOD: 'get',
    cb: (mainWindow) => {
      console.log(`-- ${TARGET}: CB`)

      const ses = session.defaultSession

      // -- COOKIES:
      const ringeoCookies = {
        // csrftoken: '9WWe7hcgrlADVJyzSgej1dOVkSJGRqlH6KTLdwAxUHsjoFQsJaWRXPD9DZDZIrJ6',
        sessionid: 'hwcvp1sqdyffuofo9pwe8la01di8ykjr',
        spAuthSessId: 'otsmo2fmpvlh71zz1tt2liqqpyy6hyi6',
        utm_source: 'localhost%3A8000',
        rc_uid: '19c5eda5-bc81-40f2-9e71-6d5ede33ad16',
        // only_existing_experiment: '1',
        io: 'meN7Gd27ujI0-U53AAAF',
        'cookie-confirmed': '1',
      }

      for (const key in ringeoCookies) {
        ses.cookies
          .set({
            value: ringeoCookies[key],
            name: key,
            url: SP_API_INSTANCE,
          })
          .then(() => {
            console.log(`set cookie OK: ${key}`)
          })
          .catch((err) => {
            console.log(`set cookie FAIL: ${key}`)
            if (err.message) console.log(err.message)
          })
      }
      // --

      const filterReqsForCustomMutations = {
        urls: ['*://pravosleva.ru/*', '*://localhost/*'],
      }
      ses.webRequest.onBeforeSendHeaders(
        filterReqsForCustomMutations,
        (details, callback) => {
          // NOTE: details.requestHeaders.Cookie already set.
          // details.requestHeaders.Origin = SP_API_INSTANCE
          details.requestHeaders['Upgrade-Insecure-Requests'] = '1'
          // details.requestHeaders.Referer = `${SP_API_INSTANCE}${details.referrer.slice(
          //   `${CONFIG[TARGET].FRONTEND_DEV_URL}/`.length - 1
          // )}`

          details.requestHeaders['User-Agent'] =
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0'

          callback({ requestHeaders: details.requestHeaders })
        }
      )

      // ses.clearCache(() => {
      //   // Some callback.
      //   console.log('-- CACHE CEARED')
      // })

      // V0:
      console.log(
        `-- ${TARGET}: LOAD URL`,
        `${CONFIG[TARGET].FRONTEND_DEV_URL}/tradein`
      )
      mainWindow.loadURL(`${CONFIG[TARGET].FRONTEND_DEV_URL}/tradein`)

      // V1:
      // mainWindow.loadFile('./index.ringeo.html')
    },
  },
}

state.set('windowCfg', {
  ...windowCfg,
  webPreferences: { ...windowCfg.webPreferences, contextIsolation: false },
})
state.set('connectedToFrontend', false)

// --- Run PM2 demon
// TODO: Clone if necessary?
// const syncClone = cmd.runSync('git clone https://github.com/RIAEvangelist/node-cmd.git');
if (RUN_PM2 === '1') {
  if (!PM2_PROCESS_NAME) {
    throw new Error('ERR: Check envs! PM2_PROCESS_NAME was not provided')
  }
  const syncData = cmd.runSync(
    `pm2 start ${path.join(
      __dirname,
      `ecosystem.dev.${TARGET.toLowerCase()}.config.js`
    )}`
  )
  if (syncData.err) {
    console.log(syncData.stderr)
    throw new Error('ERR: syncData')
  }
  console.log(syncData.data)
  pm2.list((err, list) => {
    // console.log(list)

    if (err) {
      console.log(err)
      throw new Error('ERR: pm2')
    }
    const targetProcess = list.find(({ name }) => name === PM2_PROCESS_NAME)
    if (
      !targetProcess ||
      !targetProcess?.pid ||
      !Number.isInteger(targetProcess?.pm_id)
    ) {
      console.log(list)
      throw new Error(`ERR: WTF? targetProcess is ${typeof targetProcess}`)
    }

    state.set(CONFIG[TARGET].PM2_PROCESS_KEY, targetProcess)
  })
}
// ---

// --- Menu, Tray
const menuCfg = require('./cfg.menu')

const mainMenu = Menu.buildFromTemplate(menuCfg)
function createTray() {
  const tray = new Tray('trayTemplate@2x.png')
  tray.setIgnoreDoubleClickEvents(true) // Ignore double click events for the tray icon
  tray.setContextMenu(mainMenu)
  return tray
}

// Add an item to the context menu that appears only when you click on an image
// See also: http://code-samples.space/notes/6038d8e016a4d2615f26d7c8
const flagMap = {
  en: '🇺🇸',
  ru: '🇷🇺',
}
// eslint-disable-next-line no-underscore-dangle
const _getTranslateOption = ({ text, langFromTo, visible }) => ({
  label:
    !!flagMap[langFromTo[0]] && !!flagMap[langFromTo[1]]
      ? `Translate from ${flagMap[langFromTo[0]]} ➡️ ${flagMap[langFromTo[1]]}`
      : `Translate from ${langFromTo[0].toUpperCase()} ➡️ ${langFromTo[1].toUpperCase()}`,
  visible,
  click: (e) => {
    shell.openExternal(
      `https://translate.google.com/?sl=${langFromTo[0]}&tl=${
        langFromTo[1]
      }&text=${encodeURIComponent(text)}&op=translate`
    )
  },
})
contextMenu({
  prepend: (_defaultActions, params, _browserWindow) => [
    {
      label: 'Rainbow',
      // Only show it when right-clicking images
      visible: params.mediaType === 'image',
    },
    // -- TRANSLATE:
    _getTranslateOption({
      text: params.selectionText,
      langFromTo: ['en', 'ru'],
      visible:
        params.selectionText.trim().length > 0 &&
        /^[a-zA-Z]/.test(params.selectionText.trim()),
    }),
    _getTranslateOption({
      text: params.selectionText,
      langFromTo: ['ru', 'en'],
      visible:
        params.selectionText.trim().length > 0 &&
        /[а-яА-ЯЁё]/.test(params.selectionText.trim()),
    }),
    // - SAMPLE:
    // {
    //   label: 'Search Google for “{selection}”',
    //   // Only show it when right-clicking text
    //   visible: params.selectionText.trim().length > 0,
    //   click: (e) => {
    //     // See also: https://www.electronjs.org/docs/api/shell
    //     shell.openExternal(`https://google.com/search?q=${encodeURIComponent(params.selectionText)}`)
    //   },
    // },
    // -
    // --
  ],
  labels: {
    inspect: 'Inspect',
  },
})
// ---

async function createWindow() {
  const mainWindow = new BrowserWindow(state.get('windowCfg'))
  await delay(3000)

  if (!isDev) {
    // TODO: Try to use cert from production?
    // mainWindow.loadURL(`http://localhost:${CONFIG[TARGET].EXPRESS_SERVER_PORT}`)
    throw new Error('ERR: Check createWindow() for !isDev')
  } else {
    console.log(
      `-- ${TARGET}: CALL CONFIG[${TARGET}].cb`,
      typeof CONFIG[TARGET].cb
    )

    CONFIG[TARGET].cb(mainWindow)
  }

  mainWindow.webContents.openDevTools({ mode: 'undocked' })
  // mainWindow.webContents.on('context-menu', (e) => { contextMenu.popup(mainWindow) })

  // --- Menu, Tray
  Menu.setApplicationMenu(mainMenu)
  createTray()
  // ---

  state.set('mainWindow', mainWindow)
}

// --- Inter Process Communication:
const cfgIPC = require('./cfg.ipc')

for (const channelName in cfgIPC) ipcMain.on(channelName, cfgIPC[channelName])
// ---

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  if (isDev) {
    createPollingByConditions({
      url:
        TARGET === 'RINGEO'
          ? `${CONFIG[TARGET].FRONTEND_DEV_URL}/tradein`
          : CONFIG[TARGET].FRONTEND_DEV_URL,
      method: CONFIG[TARGET].FRONTEND_FIRST_CONNECT_METHOD,
      interval: CONFIG[TARGET].FRONTEND_FIRST_CONNECT_INTERVAL,
      callbackAsResolve: () => {
        console.log(
          `🔥 SUCCESS! CONNECTED TO ${CONFIG[TARGET].FRONTEND_DEV_URL}`
        )
        state.set('connectedToFrontend', true)
        createWindow()
      },
      toBeOrNotToBe: () => !state.get('connectedToFrontend'), // NOTE: Need to try reconnect again is false
      callbackAsReject: () => {
        console.log(
          `FUCKUP! ${CONFIG[TARGET].FRONTEND_DEV_URL} IS NOT AVAILABLE YET!`
        )
        console.log(
          `TRYING TO RECONNECT in ${
            CONFIG[TARGET].FRONTEND_FIRST_CONNECT_INTERVAL / 1000
          } seconds...`
        )
      },
    })
  } else {
    createWindow()
  }
  // ---

  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  // expressApp.close(() => { console.log('Express server closed.') })

  try {
    const { pm_id } = state.get(CONFIG[TARGET].PM2_PROCESS_KEY)

    // WAY 1: Async (Dont do that!)
    // pm2.delete(pm_id, (err, proc) => {
    //   if (err) throw err
    //   console.log(proc)
    //   pm2.list((err) => {
    //     if (err) throw err
    //     // Do something...
    //   })
    // })

    // WAY 2: Sync
    const syncData = cmd.runSync(`pm2 delete ${pm_id}`)
    if (syncData.err) {
      console.log(syncData.stderr)
      throw new Error('ERR: syncData')
    }
    console.log(syncData.data)
  } catch (err) {
    console.log('May be PM2 process not found?')
    console.log(err)
  }

  // if (process.platform !== 'win32') {}
  if (process.platform !== 'darwin') app.quit()
})
