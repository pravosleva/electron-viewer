/* eslint-disable node/no-unpublished-require */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */

module.exports = [
  {
    label: '⚙️ DevTools',
    role: 'toggleDevTools',
  },
  // {
  //   role: 'toggleFullscreen',
  // },
  {
    label: 'Actions',
    submenu: [
      {
        role: 'reload',
      },
    ],
  },
  {
    label: 'Tools',
    submenu: [
      {
        label: 'Simple test',
        click: () => {
          console.log('Hello from menu cfg')
        },
      },
    ],
  },
]
