DEFAULT_RUNTIME_DATA_DIR=/home/pravosleva/projects/smartprice_projects/ssr/runtime_data
DEFAULT_DOMAIN=https://smartprice.ru
SSR_PLACEMENT_DIR_NAME=_placement.ssr
SSR_PLACEMENT_DIR=$PWD/$SSR_PLACEMENT_DIR_NAME
# EXPRESS_HELPER_DIR=_placement.express-helper
# EXPRESS_HELPER_PLACEMENT_DIR=$PWD/$EXPRESS_HELPER_DIR

if [ $# -eq 1 ]
then
  case $1 in
    "dev")

      # Step 0: Checking local SSR repo & clone if necessary
      SSR_DIR=$SSR_PLACEMENT_DIR
      echo "Chechking: $SSR_DIR"
      if [ ! -d $SSR_PLACEMENT_DIR ]
      then
        echo "Does not exists: $SSR_PLACEMENT_DIR" &&

        # Step 1: Clone SSR and install deps:
        # https://gitlab.com/smartprice/js/ssr.git
        git clone git@gitlab.com:smartprice/js/ssr.git $SSR_PLACEMENT_DIR_NAME &&
        if [ -d $SSR_DIR ]
        then
          echo "Created, exists: $SSR_DIR"
        fi
      else
        echo "Аlready exists: $SSR_PLACEMENT_DIR"
      fi

      echo "SSR deps installing..." &&
      yarn --cwd $SSR_DIR &&

      echo "SSR_DIR=$SSR_DIR
PM2_PROCESS_NAME=ssr-3000-dev" > .env.ssr &&
      echo "const fs = require('fs')

require('dotenv').config(fs.readFileSync('.env.ssr'))

const { SSR_DIR, PM2_PROCESS_NAME } = process.env

if (!SSR_DIR) throw new Error('Check envs: SSR_DIR should be provided')

module.exports = {
  apps: [
    {
      name: PM2_PROCESS_NAME,
      cwd: SSR_DIR,
      script: 'yarn',
      args: 'dev',
      interpreter: 'none',
      // env: {},
    },
  ],
}" > ecosystem.dev.ssr.config.js &&

      # Step 2: Init envs for SSR:
      echo "RUNTIME_DATA_DIR=$DEFAULT_RUNTIME_DATA_DIR
DOMAIN=$DEFAULT_DOMAIN
IS_LOCAL_FAKE_DATA_REQUIRED=true" > $SSR_DIR/.env.dev # LOG_LEVELS=info,debug
    ;;
    *)
    echo "🚫 ERR: envs-init.sh | Undefined param value" &&
    exit 1
  esac
  exit 0
else
  echo "🚫 ERR: envs-init.sh | Param is required: dev"
  exit 1
fi